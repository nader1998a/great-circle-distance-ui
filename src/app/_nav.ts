import { INavData } from '@coreui/angular';

export const navItems: INavData[] = [
  {
    name: 'Customers',
    url: '/customers',
    icon: 'icon-speedometer'
  }
];
