export interface OfficesModels {
  location    : string;
  address     : string;
  coordinates : string;
}
