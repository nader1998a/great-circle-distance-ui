import {OfficesModels} from "./offices.models";

export interface CustomersModel {
  id                  : number;
  urlName             : string;
  organization        : string;
  customerLocations   : string;
  willWorkRemotely    : boolean;
  website             : string;
  services            : string;
  offices             : OfficesModels[];
}
