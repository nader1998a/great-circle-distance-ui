import {Inject, Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  protected httpUrl: string = environment.baseUrl;

  constructor(
    @Inject(String) protected endpoint: string,
    protected http: HttpClient
  ) { }

  get<T>(customUrl?: string): Observable<T> {
    return this.http.get<T>(this.httpUrl.concat(this.endpoint, '/', customUrl ? customUrl : '' ));
  }
}
