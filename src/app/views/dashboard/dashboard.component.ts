import { Component, OnInit } from '@angular/core';
import {CustomersModel} from "../../models/customers.model";
import {Observable} from "rxjs";
import {CustomersService} from "../../services/customers/customers.service";
import {FormControl} from "@angular/forms";
import {debounceTime} from "rxjs/operators";

@Component({
  templateUrl: 'dashboard.component.html'
})
export class DashboardComponent implements OnInit {

  customers$: Observable<CustomersModel[]>;
  rangeFormControl: FormControl;
  constructor(
    private customerService: CustomersService
  ) {
  }

  ngOnInit() {
    this.getCustomers();
    this.rangeFormControl = new FormControl('');
    this.subscribeToRangeValues();
  }

  getCustomers(range?: number): void {
    this.customers$ = this.customerService.get(range ? `?range=${range}` : null);
  }

  subscribeToRangeValues(): void {
    this.rangeFormControl.valueChanges.pipe(
      debounceTime(500),
    ).subscribe(value => {
      this.getCustomers(value);
    })
  }
}
